/* 
TESTED ONLY IN THE GOOGLE CHROME

How to use it:
1. Edit config in this file to match your needs
2. Go to https://r.onliner.by/ak/ and open browser console (F12 for google chrome)
3. Copy-paste script to the console, hit enter button

Comment line 63 to prevent opening results in the separate tabs
*/

//blackList - if text on page contains it, we will block it. 191770414, 192333968 - it's bad agents license numbers
//whitelist - flats WITHOUT this stuff are acceptable for rent. Usually, there are common practice to buy fridge or other stuff to this flat as a payment for rent.
//maxLifeTime - time from ad createment 
//minNumberOfPhotos - usually not-fake ads have at least 3 photos
var config = {
	baseUrl: 'https://ak.api.onliner.by/search/apartments?bounds%5Blb%5D%5Blat%5D=53.696299938340694&bounds%5Blb%5D%5Blong%5D=27.321624755859375&bounds%5Brt%5D%5Blat%5D=54.098865472796994&bounds%5Brt%5D%5Blong%5D=27.802276611328125&rent_type[]=1_room&rent_type[]=2_rooms&rent_type[]=3_rooms&rent_type[]=4_rooms&rent_type[]=5_rooms&rent_type[]=6_rooms',
	priceMin: 1,
	priceMax: 270, //in usd
	blackList:  ['ПЛАНЕТА НЕДВИЖИМОСТИ', 'МЕГАМАСШТАБ', '191770414', '192333968', 'ДЕВУШКУ НА ПОДСЕЛЕНИЕ', '+375 29 615-71-16', '+375 29 685-85-90', '+375 29 746-60-10', '+375 29 149-46-48'],
	whiteList: ['ТЕЛЕВИЗОР', 'ЛОДЖИЯ ИЛИ БАЛКОН', 'ИНТЕРНЕТ', 'КОНДИЦИОНЕР', 'СТИРАЛЬНАЯ МАШИНА', 'ХОЛОДИЛЬНИК'],
	getUrl: function() {
		return this.baseUrl + '&price%5Bmin%5D='+ this.priceMin+'&price%5Bmax%5D='+ this.priceMax +'&currency=usd';
	},
	maxLifeTime: 0.5, //in days
	minNumberOfPhotos: 3
}
config.url = config.getUrl();
$('body')[0].innerHTML = '';

var isApartmentSuit = function(data){
	data = data.toUpperCase();
	for (var index = 0; index < config.blackList.length; index++) {
		if(data.indexOf(config.blackList[index]) > -1) return false;
	}
	var splitted = data.split('apartment-options__item_lack">'.toUpperCase());
	for (var index = 1; index < splitted.length; index++) {
		var res = false;
		for (var i = 0; i < config.whiteList.length && !res; i++) {
			if (splitted[index].startsWith(config.whiteList[i])) {
				res = true
			}
		}
		if(!res) return false;
	}

	if(data.split('apartment-cover__thumbnail'.toUpperCase()).length - 4 < config.minNumberOfPhotos) return false;

	return true;
}
var processPage = function(page){
	$.get(config.url + '&page=' + page, function(data) {
		if(data.page.current != data.page.last){
			processPage(page + 1);
		}
		for (var index = 0; index < data.apartments.length; index++) {
			(function(){
				var apartment = data.apartments[index];
				if ((new Date() - new Date(apartment.created_at))/1000/60/60/24 > config.maxLifeTime) return;
				$.get(apartment.url, function(data) {
					if (isApartmentSuit(data)) {
						$('body')[0].innerHTML += '<a href="'+ apartment.url + '">' + apartment.url +'</a><br>';
						window.open(apartment.url, '_blank'); //comment this line to prevent tabs opening
					}
				});
			})();
		}
	});	
}
processPage(1);
